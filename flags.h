#include<stdlib.h>
#include<stdbool.h>
#include<stdio.h>
#ifndef FLAGS_H_
#define FLAGS_H_

typedef unsigned int uint;

void *flags_init(uint num) {

    uint byte_divisible = num % 8 != 0;

    return malloc(num/8 + byte_divisible);

}

bool flags_get(char* flags, uint i) {

    uint byte_index = i / 8;

    uint bit_index = i % 8;

    unsigned char pattern = (unsigned char) (1 << bit_index);

    unsigned char byte = flags[byte_index];

    return (byte & pattern) != 0;

}

void flags_set(char* flags, uint i, bool value) {

    uint byte_index = i / 8;
    uint bit_index = i % 8;

    unsigned char byte = flags[byte_index];

    if(byte & (1 << bit_index) == 1) {

        if(value) {
            return;
        }

        byte = byte - (1 << bit_index);
    } else {

        if(!value) {
            return;
        }

        byte = byte + (1 << bit_index);

    }

    flags[byte_index] = byte;
}

#endif
