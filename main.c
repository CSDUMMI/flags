#include "flags.h"
#include<stdbool.h>
#include<stdio.h>

int main() {

    void *flags = flags_init(5);

    flags_set(flags, 0, true);

    printf("1 == %d\n", flags_get(flags, 0));

    flags_set(flags, 0, false);

    printf("0 == %d\n", flags_get(flags, 0));

}
